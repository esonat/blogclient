FROM frolvlad/alpine-oraclejdk8
VOLUME /tmp
ADD  build/libs/demo-0.0.1-SNAPSHOT.jar blogclient.jar
EXPOSE 9010
ENTRYPOINT ["java","-jar","/blogclient.jar"]

#ENTRYPOINT ["gradle","bootRun","$port","/blogclient.jar"]
#-Dserver.port","-Dblog.client.id","-Dblog.client.username","-Dblog.client.role
