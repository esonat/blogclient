package com.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

/**
 * Created by sonat on 14.08.2016.
 */

@Configuration
@PropertySource("classpath:application.properties")
public class BeanConfig {

    @Resource
    Environment env;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
//
//    @Bean
//    public ConnectionFactory connectionFactory(){
//        CachingConnectionFactory connectionFactory=new CachingConnectionFactory("127.0.0.1");
//        connectionFactory.setUsername("guest");
//        connectionFactory.setPassword("guest");
//        connectionFactory.setPort(5672);
//        return connectionFactory;
//    }
//
//    @Bean
//    public AmqpAdmin amqpAdmin(){
//        return new RabbitAdmin(connectionFactory());
//    }
//
//    @Bean
//    public Queue clientQueue(){
//        return new Queue(System.getProperty("blog.client.id"),false);
//    }
//
//    @Bean
//    public SimpleMessageListenerContainer messageListenerContainer(){
//        SimpleMessageListenerContainer container=
//                new SimpleMessageListenerContainer();
//        container.setConnectionFactory(connectionFactory());
//        container.setMessageListener(clientListenerAdapter());
//        container.setQueues(clientQueue());
//        return container;
//    }
//
//    @Bean
//    public MessageListenerAdapter clientListenerAdapter(){
//        MessageListenerAdapter adapter=new MessageListenerAdapter();
//        adapter.setDelegate(clientListener());
//        adapter.setDefaultListenerMethod("listen");
//        adapter.setMessageConverter(new SimpleMessageConverter());
//        return adapter;
//    }
//
//    @Bean
//    public ClientListener clientListener(){
//        return new ClientListener();
//    }

//    @Bean
//    public RabbitTemplate rabbitTemplate(){
//        RabbitTemplate rabbitTemplate=new RabbitTemplate();
//        rabbitTemplate.setConnectionFactory(connectionFactory());
//        rabbitTemplate.setMessageConverter(new SimpleMessageConverter());
//        rabbitTemplate.setRoutingKey(System.getProperty("blog.client.id"));
//
//        return rabbitTemplate;
//    }



//    @Bean
//    public SingleConnectionFactory rabbitConnFactory(){
//        SingleConnectionFactory connectionFactory=new SingleConnectionFactory();
//        Properties props=new Properties();
//        props.put("username","guest");
//        props.put("password","guest");
//        props.put("virtualHost","/");
//        props.put("port","5672");
//
//    }
////
//    @Bean(autowire = Autowire.BY_TYPE)
//    public QueueConsumer queueConsumer() throws TimeoutException,IOException {
//        String clientId=env.getProperty("blog.client.id");
//        QueueConsumer consumer = new QueueConsumer(clientId);
//        return consumer;
//    }
}
