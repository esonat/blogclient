package com.example.config;

import com.mongodb.Mongo;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by sonat on 18.08.2016.
 */

@Configuration
@EnableMongoRepositories
public class MongoConfig extends AbstractMongoConfiguration{

    @Override
    protected String getDatabaseName(){
        return "clientDB";
    }
    @Override
    public Mongo mongo() throws Exception{
        return new Mongo();
    }
    @Override
    protected String getMappingBasePackage(){
        return "com.example.entities";
    }

}
