package com.example.entities;

/**
 * Created by sonat on 13.08.2016.
 */
public class Post {
    private String text;
    private String category;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
