package com.example.entities;

/**
 * Created by sonat on 18.08.2016.
 */
public class Category {
    private String id;

    private String name;

    public Category(){}

    public Category(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return String.format(
                "Category[id=%s, name='%s']",
                id,name);
    }
}
