package com.example.scheduled;

import com.example.connection.HeartBeatData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


@PropertySource(value = "classpath:/custom.properties")
@Component
public class HeartBeatTask {

    private static Logger logger= LoggerFactory.getLogger(HeartBeatTask.class);

    @Autowired
    Environment env;



    @Scheduled(fixedDelay = 5000)
    public void send(){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            String serverURL=   env.getProperty("blog.server.url");

            HeartBeatData message=new HeartBeatData();

            message.setType("HEARTBEAT");
            message.setClientId(env.getProperty("blog.client.id"));
            message.setPort(env.getProperty("server.port"));
            message.setUsername(env.getProperty("blog.client.username"));
            message.setRole(env.getProperty("blog.client.role"));


            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();

            //Map<String,String> map=new HashMap<String,String>();
            map.add("type"      ,message.getType());
            map.add("id"        ,message.getClientId());
            map.add("port"      ,message.getPort());
            map.add("username"  ,message.getUsername());
            map.add("role"      ,message.getRole());

            HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            //HttpEntity<Map<String,String>> request=new HttpEntity<Map<String,String>>(headers);

            RestTemplate restTemplate=new RestTemplate();
            ResponseEntity<String> response=restTemplate.postForEntity(serverURL, request, String.class);

            System.out.println(response.getStatusCode());

        }catch (Exception e){
            e.printStackTrace();
            //logger.info("Heartbeat Task:"+e.getMessage()+e.printStackTrace().);
        }

        System.out.println("HEARTBEAT MESSAGE SENT");
    }
}
