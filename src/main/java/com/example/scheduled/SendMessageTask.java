package com.example.scheduled;

import com.example.connection.CategoryData;
import com.example.connection.PostData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created by sonat on 17.08.2016.
 */

@PropertySource(value = "classpath:/custom.properties")
@Component
public class SendMessageTask {
        private static Logger logger= LoggerFactory.getLogger(com.example.scheduled.HeartBeatTask.class);

        @Autowired
        Environment env;

        public MultiValueMap<String,String> prepareMessage(){
            String role=env.getProperty("blog.client.role");
            MultiValueMap<String,String> map=new LinkedMultiValueMap<String,String>();

            String[] categories=new String[]
                    {"Spring","JPA","JMS","Docker","Microservices",
                    "Kafka","Apache","Python","Web","Data"};

            switch (role) {
                case "POST":
                    try {
                        PostData data = new PostData();

                        data.setType("POST");
                        data.setClientId(env.getProperty("blog.client.id"));
                        data.setCategoryName("Java");
                        data.setDocumentContent(null);
                        data.setText("DENEME");

                        map.add("type", data.getType());
                        map.add("id", data.getClientId());
                        map.add("categoryName", data.getCategoryName());
                        map.add("documentContent", null);
                        map.add("text", data.getText());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case "CATEGORY":

                    //Random random=new Random();
                    //int r_index=random.nextInt(categories.length);

                    try {
                        CategoryData categoryData = new CategoryData();

                        //String r_name=categories[r_index];

                        categoryData.setClientId(env.getProperty("blog.client.id"));
                        categoryData.setType("CATEGORY");
                    /* categoryData.setName(r_name); */
                        categoryData.setName("Java");

                        map.add("type", categoryData.getType());
                        map.add("name", categoryData.getName());
                        map.add("id", categoryData.getClientId());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
            return map;
        }



        @Scheduled(fixedDelay = 15000)
        public void send(){
            try {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

                String serverURL=   env.getProperty("blog.server.url");

                //PostData message=new PostData();


                MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
                map=prepareMessage();

                //Map<String,String> map=new HashMap<String,String>();

                HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
                //HttpEntity<Map<String,String>> request=new HttpEntity<Map<String,String>>(headers);

                RestTemplate restTemplate=new RestTemplate();
                ResponseEntity<String> response=restTemplate.postForEntity(serverURL, request, String.class);

                System.out.println(response.getStatusCode());

            }catch (Exception e){
                e.printStackTrace();
                //logger.info("Heartbeat Task:"+e.getMessage()+e.printStackTrace().);
            }

            System.out.println("POST MESSAGE SENT");
        }
}
