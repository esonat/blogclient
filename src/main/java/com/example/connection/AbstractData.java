package com.example.connection;

import java.util.Map;

/**
 * Created by sonat on 17.08.2016.
 */
public abstract class AbstractData {
    String clientId;
    String type;
    Map<String,Object> message;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getMessage() {
        return message;
    }

    public void setMessage(Map<String, Object> message) {
        this.message = message;
    }
}
