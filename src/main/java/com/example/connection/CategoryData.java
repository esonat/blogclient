package com.example.connection;

import com.example.entities.Category;

import java.io.Serializable;

public class CategoryData extends AbstractData implements Serializable {

    private static final long serialVersionUID = -3711768946070248971L;
    private String name;

    public CategoryData(){}

    public CategoryData(Object object){
        type="Category";
        name=((Category)object).getName();
        message.put(type,name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}