package com.example.message.entities;

import java.io.Serializable;

public class MessageObject implements Serializable {

   private static final long serialVersionUID = 816962767581721622L;
    String type;
    Object message;


    public MessageObject(){}

    public MessageObject(String type,Object message){
        this.type=type;
        this.message=message;
    }
    public String getType(){
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }
}
