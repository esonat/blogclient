package com.example.message;

import com.example.entities.Category;
import com.example.message.entities.MessageObject;
import com.example.repository.RepositoryManager;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;


public class QueueConsumer extends EndPoint implements Runnable,Consumer,QueueConsumerInterface {
    Logger logger= LoggerFactory.getLogger(QueueConsumer.class);
    private static String LNAME="QUEUECONSUMER";

    public QueueConsumer(String endPointName) throws IOException ,TimeoutException{
        super(endPointName);
    }

    public void run() {
        try {
            channel.basicConsume(endPointName, true,this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleConsumeOk(String consumerTag) {
        System.out.println("Consumer "+consumerTag +" registered");
    }


    public void handleDelivery(String consumerTag, Envelope env,
                               BasicProperties props, byte[] body) throws IOException {

        String MNAME="HANDLE DELIVERY";
        System.out.println("HANDLE DELIVERY");

        MessageObject message=(MessageObject)SerializationUtils.deserialize(body);

        switch (message.getType()){
            case "Category":

                        String categoryName = (String) message.getMessage();
                        System.out.println("RECEIVED CATEGORY MESSAGE:" + categoryName);
                    try {
                        RepositoryManager repositoryManager = new RepositoryManager();
                        repositoryManager.save(new Category(categoryName));

                    }catch (Exception e){
                        System.out.println(LNAME+MNAME+"Error saving category in repository");
                    }

            break;
            case "CategoryList":
                try {
                    List<String> categorynames = (List<String>) message.getMessage();
                    System.out.println("RECEIVED CATEGORYLIST MESSAGE");

                    RepositoryManager repositoryManager=new RepositoryManager();

                    try {
                        repositoryManager.deleteCategories();
                    }catch (Exception e){
                        System.out.println(LNAME+MNAME+"Error deleting category from repository");
                        logger.error("Error deleting category from repository");
                    }

                    if (categorynames != null) {
                        for (String name : categorynames) {
                            try {
                                repositoryManager.save(new Category(name));
                            }catch (Exception e){
                                System.out.println(LNAME+MNAME+"Error saving category in repository");
                                logger.error("Error saving category in repository");
                            }
                        }
                    }
                }catch (Exception e){
                    System.out.println(LNAME+MNAME+"Error in CategoryList");
                    logger.error("Error in CategoryList");
                }

                break;
            case "ClientRole":
                    String role=(String)message.getMessage();
                    System.out.println("RECEIVED CLIENTROLE MESSAGE:"+role);
                    System.setProperty("blog.client.role",role);
                    System.out.println("SYSTEM PROPERTY SET:"+System.getProperty("blog.client.role"));
            break;
        }
    }

    public void handleCancel(String consumerTag) {}
    public void handleCancelOk(String consumerTag) {}
    public void handleRecoverOk(String consumerTag) {}
    public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {}
}