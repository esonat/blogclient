package com.example.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by sonat on 18.08.2016.
 */

@PropertySource(value = "classpath:/custom.properties")
@Component
public class MessageManager {
    private Logger logger= LoggerFactory.getLogger(MessageManager.class);

    @Resource
    Environment env;

    public MessageManager(){}


    public void listen(){
        try {
            String clientId=env.getProperty("blog.client.id");
            QueueConsumer consumer=null;

            try {
                consumer = new QueueConsumer(clientId);

            }catch (IOException e){
                System.out.println("Error initializing QueueConsumer"+e.getCause()+e.getMessage());
                logger.error("Error initializing QueueConsumer"+e.getCause()+e.getMessage());

            }catch(Exception e){
                System.out.println("Error initializing QueueConsumer");
                logger.error("Error initializing QueueConsumer");
            }
            Thread consumerThread=null;

            try {
                consumerThread = new Thread(consumer);

            }catch (Exception e){
                System.out.println("Error initializing consumerThread");
                logger.error("Error initializing consumerThread");
            }
            try {
                consumerThread.start();

            }catch (Exception e){
                System.out.println("Error starting consumerThread");
                logger.error("Error starting consumerThread");
            }

        }catch (Exception e){
            System.out.println("Error in listen");
            logger.error("Error in listen");

        }
    }
}
