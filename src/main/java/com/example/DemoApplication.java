package com.example;

import com.example.message.QueueConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@EnableScheduling
@ComponentScan("com.example")
@SpringBootApplication
public class DemoApplication {

    @Autowired
    Environment env;

    public static void main(String[] args) {

        if (args.length > 0) {
            if (args[0] != null) System.setProperty("server.port", args[0]);
            if (args[1] != null) System.setProperty("blog.client.id", args[1]);
            if (args[2] != null) System.setProperty("blog.client.username", args[2]);
            if (args[3] != null) System.setProperty("blog.client.role", args[3]);
        }
        SpringApplication.run(DemoApplication.class, args);

        try {
            String clientId=System.getProperty("blog.client.id");
            QueueConsumer consumer = new QueueConsumer(clientId);

            Thread consumerThread = new Thread(consumer);
            consumerThread.start();
        }
        catch (TimeoutException e){
            e.printStackTrace();}
        catch (IOException e){
            e.printStackTrace();
        }
    }
}

