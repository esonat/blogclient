package com.example.repository;

import com.example.entities.Category;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;

public class RepositoryManager implements RepositoryManagerInterface{

    public void save(Category category){
        try {
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Now connect to your databases
            DB db = mongoClient.getDB("clientDB");
            DBCollection coll = db.getCollection("category");
            System.out.println("Collection mycol selected successfully");

            BasicDBObject doc=new BasicDBObject().append("name",category.getName());

            coll.insert(doc);
            System.out.println("Document inserted successfully");

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    public void deleteCategories(){
        try{
        MongoClient mongoClient = new MongoClient("localhost", 27017);

        // Now connect to your databases
        DB db = mongoClient.getDB("clientDB");
        DBCollection coll = db.getCollection("category");


        BasicDBObject document = new BasicDBObject();

        // Delete All documents from collection Using blank BasicDBObject
        coll.remove(document);

        }catch (UnknownHostException e){
            System.out.println(e.getStackTrace());
        }
    }

  //  public void save(Category category){
     ///   categoryRepository.save(category);
    //}

    //public Category findByName(String name){
//        return categoryRepository.findByName(name);
//    }
}
